package org.jdragon.springz.web.core.configuration;

import lombok.Data;

import java.util.LinkedList;
import java.util.List;

/**
 * @Author JDragon
 * @Date 2021.03.05 下午 3:31
 * @Email 1061917196@qq.com
 * @Des:
 */
@Data
public class WebFilterRegistry {

    private final List<WebFilterRegistration> webFilterRegistrations = new LinkedList<>();

    public WebFilterRegistration addWebFilter(WebFilter webFilter) {
        WebFilterRegistration webFilterRegistration = new WebFilterRegistration(webFilter);
        webFilterRegistrations.add(webFilterRegistration);
        return webFilterRegistration;
    }
}
