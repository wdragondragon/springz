package org.jdragon.springz.web.core.configuration;

import java.util.LinkedList;
import java.util.List;

/**
 * @Author JDragon
 * @Date 2021.03.05 下午 11:35
 * @Email 1061917196@qq.com
 * @Des:
 */
public class ResourceMappingRegistry {
    private final List<ResourceMappingRegistration> resourceMappingRegistrations = new LinkedList<>();

    public ResourceMappingRegistration addResourceMapping(String pathPattern) {
        ResourceMappingRegistration resourceMappingRegistration = new ResourceMappingRegistration(pathPattern);
        resourceMappingRegistrations.add(resourceMappingRegistration);
        return resourceMappingRegistration;
    }
}
