package org.jdragon.springz.web.core.resolver;

import org.jdragon.springz.web.core.entity.RequestParams;

import java.lang.reflect.Parameter;

/**
 * @Author: Jdragon
 * @email: 1061917196@qq.com
 * @Date: 2020.11.14 14:25
 * @Description:
 */
public interface ParameterResolver {
    /**
     * Process method parameters
     *
     * @param requestParams Target method related information
     * @param parameter    The parameter of the target method
     * @return Specific values ​​corresponding to the parameters of the target method
     */
    Object resolve(RequestParams requestParams, Parameter parameter);
}
