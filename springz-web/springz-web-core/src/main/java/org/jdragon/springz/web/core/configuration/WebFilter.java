package org.jdragon.springz.web.core.configuration;

import org.jdragon.springz.web.core.entity.HttpRequest;
import org.jdragon.springz.web.core.entity.HttpResponse;

/**
 * @Author JDragon
 * @Date 2021.03.03 下午 3:50
 * @Email 1061917196@qq.com
 * @Des:
 */
public interface WebFilter {
    void filter(HttpRequest httpRequest, HttpResponse httpResponse, WebFilterChain webFilterChain);
}
