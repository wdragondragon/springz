package org.jdragon.springz.web.core.entity;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: Jdragon
 * @email: 1061917196@qq.com
 * @Date: 2020.11.16 22:12
 * @Description:
 * <p>
 * 用于在controller中注入，与视图通信的类
 * 通常
 * 向controller输入 {@link WebRequest#requestParams 输入参数}
 * 向view输出 {@link WebRequest#attributes
 * </p>
 */
@Data
public class WebRequest {

    private final RequestParams requestParams;

    private final Map<String, Object> attributes = new HashMap<>();

    public WebRequest(RequestParams requestParams) {
        this.requestParams = requestParams;
    }
}
