package org.jdragon.springz.web.core.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpUtil;
import org.jdragon.springz.core.annotation.Component;
import org.jdragon.springz.utils.http.response.Result;
import org.jdragon.springz.utils.http.response.ResultCode;
import org.jdragon.springz.utils.json.JsonUtils;
import org.jdragon.springz.web.core.entity.HttpResponse;
import org.jdragon.springz.web.core.entity.ResponseData;
import org.jdragon.springz.web.core.netty.ContentType;

/**
 * @Author JDragon
 * @Date 2021.03.04 上午 12:12
 * @Email 1061917196@qq.com
 * @Des:
 */

@Component("notFoundServerHandler")
public class NotFoundHandler extends SimpleChannelInboundHandler<FullHttpRequest> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest msg) throws Exception {
        HttpResponse httpResponse = new HttpResponse(ctx, HttpUtil.isKeepAlive(msg));
        Result<String> error = Result.build(ResultCode.NOT_FOUND);
        ResponseData responseData = new ResponseData(JsonUtils.object2Byte(error), ContentType.APPLICATION_JSON);
        httpResponse.write(responseData);
    }
}
