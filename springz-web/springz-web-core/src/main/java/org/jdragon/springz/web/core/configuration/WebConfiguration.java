package org.jdragon.springz.web.core.configuration;

import lombok.Data;
import org.jdragon.springz.web.core.configuration.WebFilterRegistry;
import org.jdragon.springz.web.core.entity.HttpProperty;


/**
 * @Author JDragon
 * @Date 2021.03.05 下午 2:47
 * @Email 1061917196@qq.com
 * @Des:
 */

@Data
public abstract class WebConfiguration {

    private HttpProperty httpProperty;

    protected abstract void addWebFilters(WebFilterRegistry webFilterRegistry);

    protected abstract void addResourceMapping(ResourceMappingRegistry resourceMappingRegistry);

    public void init() {
        WebFilterRegistry webFilterRegistry = new WebFilterRegistry();
        ResourceMappingRegistry resourceMappingRegistry = new ResourceMappingRegistry();
        addWebFilters(webFilterRegistry);
        addResourceMapping(resourceMappingRegistry);
    }


}
