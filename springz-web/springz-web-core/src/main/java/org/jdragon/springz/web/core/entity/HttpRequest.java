package org.jdragon.springz.web.core.entity;

import io.netty.handler.codec.http.HttpMethod;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jdragon.springz.web.annotation.RequestMethod;

import java.net.InetSocketAddress;

/**
 * @Author JDragon
 * @Date 2021.03.03 下午 4:22
 * @Email 1061917196@qq.com
 * @Des:
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HttpRequest {

    private String path;

    private String uri;

    private String matchingPath;

    private RequestMethod method;

    private RequestParams requestParams;

    private InetSocketAddress remoteAddress;

    private InetSocketAddress localAddress;
}
