package org.jdragon.springz.web.core.configuration;

import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author JDragon
 * @Date 2021.03.05 下午 3:25
 * @Email 1061917196@qq.com
 * @Des:
 */

@Data
public class WebFilterRegistration {

    private final WebFilter webFilter;

    private final List<String> includePatterns = new ArrayList<>();

    private final List<String> excludePatterns = new ArrayList<>();

    public WebFilterRegistration(WebFilter webFilter) {
        this.webFilter = webFilter;
    }

    public WebFilterRegistration addPathPatterns(String... patterns) {
        return addPathPatterns(Arrays.asList(patterns));
    }

    public WebFilterRegistration addExcludePatterns(String... patterns) {
        return addExcludePatterns(Arrays.asList(patterns));
    }

    public WebFilterRegistration addPathPatterns(List<String> paths) {
        this.includePatterns.addAll(paths);
        return this;
    }

    public WebFilterRegistration addExcludePatterns(List<String> paths) {
        this.excludePatterns.addAll(paths);
        return this;
    }
}
