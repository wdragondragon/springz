package org.jdragon.springz.web.core.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.*;
import org.apache.commons.codec.CharEncoding;
import org.apache.commons.codec.Charsets;
import org.jdragon.springz.utils.http.response.Result;
import org.jdragon.springz.utils.json.JsonUtils;
import org.jdragon.springz.web.annotation.RequestMethod;
import org.jdragon.springz.web.core.RouteMethodMapper;
import org.jdragon.springz.web.core.entity.HttpRequest;
import org.jdragon.springz.web.core.entity.HttpResponse;
import org.jdragon.springz.web.core.entity.RequestParams;
import org.jdragon.springz.web.core.entity.ResponseData;
import org.jdragon.springz.web.core.entity.RouteInfo;
import org.jdragon.springz.web.core.utils.AnalyzeParamInvoke;


/**
 * @Author: Jdragon
 * @email: 1061917196@qq.com
 * @Date: 2020.10.29 20:22
 * @Description:
 */
public class DefaultWebHandler implements WebHandler {
    @Override
    public void handle(HttpRequest httpRequest, HttpResponse httpResponse) {
        RequestMethod requestMethod = httpRequest.getMethod();
        String matchingPath = httpRequest.getMatchingPath();
        RequestParams requestParams = httpRequest.getRequestParams();

        RouteInfo routeInfo = RouteMethodMapper.getRoute(requestMethod, matchingPath);
        ResponseData responseData = AnalyzeParamInvoke.get().invokeMethod(routeInfo, requestParams);

        httpResponse.write(responseData);
    }
}
