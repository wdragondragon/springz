package org.jdragon.springz.web.core.configuration;

import lombok.Getter;

/**
 * @Author JDragon
 * @Date 2021.03.05 下午 11:19
 * @Email 1061917196@qq.com
 * @Des:
 */
@Getter
public class ResourceMappingRegistration {

    private final String pathPattern;

    private String resourceLocation;

    public ResourceMappingRegistration(String pathPattern) {
        this.pathPattern = pathPattern;
    }

    public void setResourceLocation(String resourceLocation) {
        this.resourceLocation = resourceLocation;
    }
}
