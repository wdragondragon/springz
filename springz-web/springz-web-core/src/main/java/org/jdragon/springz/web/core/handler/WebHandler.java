package org.jdragon.springz.web.core.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import org.jdragon.springz.web.core.entity.HttpRequest;
import org.jdragon.springz.web.core.entity.HttpResponse;

/**
 * @Author: Jdragon
 * @email: 1061917196@qq.com
 * @Date: 2020.10.29 20:10
 * @Description: 获得已封装的HttpRequest, HttpResponse去处理的处理器
 */
public interface WebHandler {
    void handle(HttpRequest httpRequest, HttpResponse httpResponse);
}
