package org.jdragon.springz.web.core.resolver;

import org.jdragon.springz.utils.json.JsonUtils;
import org.jdragon.springz.web.core.entity.RequestParams;

import java.lang.reflect.Parameter;

/**
 * @Author: Jdragon
 * @email: 1061917196@qq.com
 * @Date: 2020.11.14 14:26
 * @Description:
 */
public class RequestBodyParameterResolver implements ParameterResolver {
    @Override
    public Object resolve(RequestParams requestParams, Parameter parameter) {
        String body = requestParams.getBody();
        if (body == null || body.isEmpty()) {
            return null;
        } else {
            return JsonUtils.json2Object(requestParams.getBody(), parameter.getType());
        }
    }
}
