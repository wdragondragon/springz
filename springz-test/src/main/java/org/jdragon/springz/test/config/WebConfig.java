package org.jdragon.springz.test.config;

import org.jdragon.springz.core.annotation.Configuration;
import org.jdragon.springz.core.annotation.Resource;
import org.jdragon.springz.web.core.configuration.ResourceMappingRegistry;
import org.jdragon.springz.web.core.configuration.WebConfiguration;
import org.jdragon.springz.web.core.configuration.WebFilter;
import org.jdragon.springz.web.core.configuration.WebFilterRegistry;

/**
 * @Author JDragon
 * @Date 2021.03.05 下午 3:35
 * @Email 1061917196@qq.com
 * @Des:
 */
@Configuration
public class WebConfig extends WebConfiguration {

    @Resource
    private WebFilter testFilter;

    @Override
    protected void addWebFilters(WebFilterRegistry webFilterRegistry) {
        webFilterRegistry.addWebFilter(testFilter).addPathPatterns("/**").addExcludePatterns("/login");
    }

    @Override
    protected void addResourceMapping(ResourceMappingRegistry resourceMappingRegistry) {
        resourceMappingRegistry.addResourceMapping("/static/**").setResourceLocation("C:/dev/static/**");
    }
}
