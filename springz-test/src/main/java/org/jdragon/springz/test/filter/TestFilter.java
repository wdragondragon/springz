package org.jdragon.springz.test.filter;

import lombok.extern.slf4j.Slf4j;
import org.jdragon.springz.core.annotation.Component;
import org.jdragon.springz.web.core.entity.HttpRequest;
import org.jdragon.springz.web.core.entity.HttpResponse;
import org.jdragon.springz.web.core.configuration.WebFilter;
import org.jdragon.springz.web.core.configuration.WebFilterChain;

/**
 * @Author JDragon
 * @Date 2021.03.03 下午 7:14
 * @Email 1061917196@qq.com
 * @Des:
 */
@Slf4j
@Component
public class TestFilter implements WebFilter {
    @Override
    public void filter(HttpRequest httpRequest, HttpResponse httpResponse, WebFilterChain webFilterChain) {
        log.debug("测试拦截器");
        webFilterChain.filter(httpRequest, httpResponse);
    }
}
