package org.jdragon.springz.core.register;


import org.jdragon.springz.core.infuse.Infuser;
import org.jdragon.springz.scanner.BeanContainer;
import org.jdragon.springz.scanner.entry.BeanInfo;
import org.jdragon.springz.scanner.entry.ClassInfo;
import org.jdragon.springz.scanner.entry.WaitBeanInfo;
import org.jdragon.springz.utils.Log.Logger;
import org.jdragon.springz.utils.Log.LoggerFactory;
import org.jdragon.springz.utils.StrUtil;

import java.util.*;

/**
 * @Author: Jdragon
 * @email: 1061917196@qq.com
 * @Date: 2020.10.21 22:26
 * @Description:
 */
public abstract class Registrar {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 放置已注册组件
     */
    protected final Map<String, BeanInfo> beanMap = BeanContainer.getBeanMap();

    /**
     * 放置因依赖缺失的等待注册组件
     */
    protected final List<WaitBeanInfo> waitBeanList = BeanContainer.getWaitBeanList();

    /**
     * 注册时，完全可使用classInfo中的属性
     *
     * @param classInfo 扫描包时提供的类信息
     * @param obj       注册实例
     * @param scope     注册策略
     */
    protected void register(ClassInfo classInfo, Object obj, String scope) {
        BeanInfo beanInfo = new BeanInfo(obj, scope, classInfo.getClazz());
        register(classInfo.getDefinitionName(), beanInfo);
    }

    /**
     * 注册时，可自定义组件名
     *
     * @param definitionName 自定义组件名
     * @param classInfo      扫描包时提供的类信息
     * @param obj            注册实例
     * @param scope          注册策略
     */
    protected void register(ClassInfo classInfo, String definitionName, Object obj, String scope) {
        BeanInfo beanInfo = new BeanInfo(obj, scope, classInfo.getClazz());
        register(definitionName, beanInfo);
    }

    /**
     * @param clazz          自定义类
     * @param definitionName 自定义组件名
     * @param obj            注册实例
     * @param scope          注册策略
     */
    protected void register(Class<?> clazz, String definitionName, Object obj, String scope) {
        BeanInfo beanInfo = new BeanInfo(obj, scope, clazz);
        register(definitionName, beanInfo);
    }

    /**
     * 公共注册类，都要调用来做递归进行父类或接口注册
     */
    protected void register(BeanInfo beanInfo) {
        BeanInfo newBean = BeanInfo.copy(beanInfo);
        Class<?> clazz = beanInfo.getClazz();
        Class<?>[] interfaces = clazz.getInterfaces();

        for (Class<?> anInterface : interfaces) {
            String interfaceSimpleName = anInterface.getSimpleName();
            newBean.setRealClass(anInterface);
            register(StrUtil.firstLowerCase(interfaceSimpleName), newBean);
        }

        Class<?> superclass = clazz.getSuperclass();

        if (superclass != null && !superclass.equals(Object.class)) {
            String superclassSimpleName = superclass.getSimpleName();
            newBean.setRealClass(superclass);
            register(StrUtil.firstLowerCase(superclassSimpleName), newBean);
        }
    }

    /**
     * @params: [definitionName, obj]
     * @return: void
     * @Description: 通用注册类，将definitionName作为key,obj作为value存到beanMap中
     **/
    private void register(String definitionName, BeanInfo regBeanInfo) {
        String className = regBeanInfo.getClazz().getName();
        //检查definitionName是否存在
        if (beanMap.containsKey(definitionName)) {
            BeanInfo beanInfo = beanMap.get(definitionName);
            //相同时则不报异常
            if (beanInfo.getClassName().equals(className)) return;
            if (!regBeanInfo.getRealClass().equals(regBeanInfo.getClazz())) return;
            logger.warn("已存在键名[键名][冲突类名]", definitionName, beanInfo.getClassName());
            logger.warn("请解决类键名冲突[键名][类名]", definitionName, className);
            return;
        }
        regBeanInfo.setDefinedName(definitionName);
        //将对象放到map容器
        beanMap.put(definitionName, regBeanInfo);
        if (beanMap.containsKey(definitionName)) {
            logger.info("注册bean成功[键名][类名]", definitionName, className);
        }
        register(regBeanInfo);
        awakeWaitBeansByDefinitionName(definitionName);
    }


    public void addWaitBean(WaitBeanInfo waitBeanInfo) {
        waitBeanList.add(waitBeanInfo);
    }

    /**
     * 根据这次注册的组件名来唤醒等待队列出队
     *
     * @param definitionName 该次注册的组件名
     */
    private void awakeWaitBeansByDefinitionName(String definitionName) {
        List<WaitBeanInfo> needAwakeBeans = getNeedAwakeBean(definitionName);
        for (WaitBeanInfo needAwakeBean : needAwakeBeans) {
            awakeWaitBean(needAwakeBean);
        }
        waitBeanList.removeAll(needAwakeBeans);
    }

    /**
     * 获取需要唤醒的组件列表
     * only call by there {@link this#awakeWaitBeansByDefinitionName(String)}
     *
     * @param definitionName 该次注册的组件名
     * @return 获取需要唤醒WaitBean
     */
    private List<WaitBeanInfo> getNeedAwakeBean(String definitionName) {
        List<WaitBeanInfo> needAwakeBeans = new ArrayList<>();

        for (WaitBeanInfo waitBeanInfo : waitBeanList) {
            List<String> needBeanName = waitBeanInfo.getNeedBeanName();
            if (needBeanName.remove(definitionName) && needBeanName.isEmpty()) {
                needAwakeBeans.add(waitBeanInfo);
            }
        }
        return needAwakeBeans;
    }

    /**
     * 唤醒等待注册的bean
     * only call by there {@link this#awakeWaitBeansByDefinitionName(String)}
     *
     * @param waitBeanInfo 需要唤醒的组件信息
     */
    private void awakeWaitBean(WaitBeanInfo waitBeanInfo) {
        List<String> paramsNameList = waitBeanInfo.getParamsNameList();

        Infuser infuser = new Infuser();

        Object[] needBean = paramsNameList.stream()
                .map(e -> infuser.createAnalyzeBean(e, e.getClass()))
                .toArray();
        Object bean = waitBeanInfo.createBean(needBean);

        String beanName = waitBeanInfo.getBeanName();
        logger.warn("唤醒出列:" + beanName);
        if (bean == null) return;

        BeanInfo beanInfo = new BeanInfo(bean, waitBeanInfo.getScope(), waitBeanInfo.getClazz());
        register(beanName, beanInfo);
    }
}