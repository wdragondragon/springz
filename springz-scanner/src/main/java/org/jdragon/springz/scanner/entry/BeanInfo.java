package org.jdragon.springz.scanner.entry;

/**
 * @Author: Jdragon
 * @email: 1061917196@qq.com
 * @Date: 2020.04.26 18:39
 * @Description: bean的信息
 */
public class BeanInfo implements Cloneable {

    public final static String SINGLETON = "singleton";

    public final static String PROTOTYPE = "prototype";

    private String definedName;

    private final Object bean;

    private final String scope;

    /**
     * 注册bean的类，bean.getClass()
     */
    private final Class<?> clazz;

    /**
     * 注册时真实的类，（有可能是bean的父类或接口）
     */
    private Class<?> realClass;

    private final String className;

    public static BeanInfo copy(BeanInfo beanInfo) {
        return new BeanInfo(beanInfo.getBean(), beanInfo.getScope(), beanInfo.getClazz());
    }

    public BeanInfo(Object bean, String scope, Class<?> clazz) {
        this(bean, scope, clazz, clazz);
    }

    public BeanInfo(Object bean, String scope, Class<?> clazz, Class<?> realClass) {
        this.bean = bean;
        this.scope = scope;
        this.clazz = clazz;
        this.realClass = realClass;
        this.className = clazz.getName();
    }

    public void setDefinedName(String definedName) {
        this.definedName = definedName;
    }

    public Object getBean() {
        return bean;
    }


    public String getScope() {
        return scope;
    }

    public boolean isSingleton() {
        return SINGLETON.equals(scope);
    }

    public boolean isPrototype() {
        return PROTOTYPE.equals(scope);
    }

    public String getClassName() {
        return className;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public String getDefinedName() {
        return definedName;
    }

    public void setRealClass(Class<?> realClass) {
        this.realClass = realClass;
    }

    public Class<?> getRealClass() {
        return realClass;
    }
}
